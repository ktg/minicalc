// (C) 2013-2021 KIM Taegyoon
// Postfix Calculator

#include <iostream>
#include <sstream>
#include <cstdio>
#include <list>
#include <cstdlib>
#include <cmath>
using namespace std;

list<double> stk;

double pop(list<double> &stk) {
	if (stk.empty()) {
		return 0.0;
	}
	double r = stk.back();
	stk.pop_back();
	return r;
}

int main() {
	string line;	
	puts("minicalc: A Postfix Calculator\n(C) 2013-2021 KIM Taegyoon (https://bitbucket.org/ktg/minicalc)");
	puts("+ - * / sqrt ^");
	while (true) {
		printf("> ");
		if (!stk.empty()) {
			double r = stk.back();
			printf("%.16g ", r);
			stk.clear();
			stk.push_back(r);
		}
		if (!getline(cin, line)) break;
		stringstream ss(line);
		string tok;
		while (getline(ss, tok, ' ')) {
			if (tok.size() == 0) continue;
			if (isdigit(tok[0]) || (tok.length() >= 2 && tok[0] == '-')) { // number
				stk.push_back(atof(tok.c_str()));
			} else if (tok == "+") {
				double a2 = pop(stk);
				double a1 = pop(stk);
				stk.push_back(a1 + a2);
			} else if (tok == "-") {
				double a2 = pop(stk);
				double a1 = pop(stk);
				stk.push_back(a1 - a2);
			} else if (tok == "*") {
				double a2 = pop(stk);
				double a1 = pop(stk);
				stk.push_back(a1 * a2);
			} else if (tok == "/") {
				double a2 = pop(stk);
				double a1 = pop(stk);
				stk.push_back(a1 / a2);
			} else if (tok == "sqrt") {
				double a1 = pop(stk);
				stk.push_back(sqrt(a1));
			} else if (tok == "^") {
				double a2 = pop(stk);
				double a1 = pop(stk);
				stk.push_back(pow(a1, a2));
			}
		}
	}
}
